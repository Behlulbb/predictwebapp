from django.db import models

class Task(models.Model):
    name = models.CharField(max_length=120)
    datafile = models.CharField(max_length=120)
    prediction = models.CharField(max_length=120)

    def __str__(self):
        return self.name
import pandas as pd
import csv
import numpy as np
import json

df_predict = pd.read_csv("prediction.csv")
data = np.array(df_predict)

neopolitanData = data[0:31]
chicagoData = data[31:62]
sicilianData = data[62:93]
tomatoData = data[93:124]

names = ["jan18", "feb18", "march18", "april18", "may18", "june18", "july18", "aug18", "sep18", "oct18", "nov18", "dec18" ]
prediction = []
def predict(data):
	for i in range(0,12):
		if(i < 7):
			sumData = int(data[i][0][19:22]) + int(data[i+12][0][19:22]) + int(data[i+24][0][19:22])
			outputValue = sumData // 3 
			prediction.append({
				"names": names[i],
				"orj": int(data[24+i][0][19:22]),
				"predict": outputValue
			})
		else:
			sumData = int(data[i][0][19:22]) + int(data[i+12][0][19:22])
			outputValue = sumData // 2
			prediction.append({
				"names": names[i],
				"predict": outputValue,
			})
	# print(prediction)	
	with open("predictData.json".format(i), 'w') as outfile:
		json.dump(prediction, outfile)
predict(neopolitanData)
predict(chicagoData)
predict(sicilianData)
predict(tomatoData)
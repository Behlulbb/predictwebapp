import React, { Component } from 'react';
import './App.css';
import 'antd/dist/antd.css';
import Tasks from './components/Tasks';
import { connect } from 'react-redux';
import * as actions from './store/actions/auth';

// function App() {
//   return (
//     <div className="App">
//       <Tasks {...this.props} />
//     </div>
//   );
// }
// const mapStateToProps = state => {
//   return {
//     isAuthenticated: state.token !== null
//   }
// }
// export default connect(mapStateToProps)(App);

class App extends Component {

  componentDidMount() {
    this.props.onTryAutoSignup();
  }

  render() {
    return (
      <div className="App">
        <Tasks {...this.props} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.token !== null
  }
}
const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignup: () => dispatch(actions.authCheckState())
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(App);
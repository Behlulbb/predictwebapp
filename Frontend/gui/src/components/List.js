import React, { Component } from 'react';
import { Button, Popconfirm, message, Icon } from 'antd';
import { Link } from 'react-router-dom';
import axios from 'axios';

class List extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tasks: props.data,
        }
    }

    deleteTask(index, e) {
        axios.delete(`http://127.0.0.1:8000/api/${index}`)
            .then(res => window.location.reload(true));
        message.success('Click on Yes');
        console.log(index)
    }

    cancel(e) {
        console.log(e);
        message.error('No deleted');
    }
    render() {
        const { tasks } = this.state;
        return (

            <div>
                <div className="taskname">
                    Task List
                </div>
                <div className="dashboard">
                    <table className="dashboard">
                        <thead className="Table-head">
                            <tr >
                                <th>ID</th>
                                <th>Task name</th>
                                <th>Data File</th>
                                <th>Prediction</th>
                                <th>Action</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody className="Table-body">
                            {tasks.map((result) => (
                                <tr key={result.id}>
                                    <td>
                                        <span className="textList">{result.id}</span>
                                    </td>
                                    <td>
                                        <span className="textList">{result.name}</span>
                                    </td>
                                    <td>
                                        <span className="textList">{result.datafile}.xlsx</span>
                                    </td>
                                    <td>
                                        <span className="textList">{result.prediction}</span>
                                    </td>
                                    <td>
                                        <Link to={'/result/' + result.id}>
                                            <Button type="primary">Result</Button>
                                        </Link>
                                    </td>
                                    <td>
                                        <Popconfirm
                                            title="Are you sure delete this task?"
                                            onConfirm={(e) => this.deleteTask(result.id, e)}
                                            onCancel={this.cancel}
                                            okText="Yes"
                                            cancelText="No"
                                        >
                                            <Icon type="delete" style={{ fontSize: '17px' }} theme="twoTone" twoToneColor="#ff4d4f" />
                                        </Popconfirm>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default List;

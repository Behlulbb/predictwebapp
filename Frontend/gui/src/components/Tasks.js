import React, { Component } from 'react';
import axios from 'axios';
import Header from './Header';
import List from './List';
import Result from './Result';
import CreateTask from './CreateTask';
import Login from './Login';
import Signup from './Signup';
import HomePage from './HomePage';

import { BrowserRouter as Router, Route } from 'react-router-dom';
const apiUrl = 'http://127.0.0.1:8000';

class Tasks extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tasks: [],
        }
    }
    componentDidMount() {
        this.listGetAll();
    }

    listGetAll = () => {
        axios
            .get(`${apiUrl}/api`)
            .then((response) => {
                this.setState({
                    tasks: response.data,
                });
            });
    }

    render() {
        const { tasks } = this.state;
        const Listprops = () => <List data={tasks} />
        const CreateTaskprops = () => <CreateTask data={tasks} listGetAll={this.listGetAll} />
        if (this.props.isAuthenticated === true) {
            return (
                <React.Fragment>
                    <Router>
                        <Header {...this.props} />
                        <Route path="/create/" component={CreateTaskprops} />
                        <Route path="/result/:id" component={Result} />
                        <Route exact path="/" component={Listprops} />
                    </Router>
                </React.Fragment>
            )
        }
        if (this.props.isAuthenticated === false) {
            return (
                <React.Fragment>
                    <Router>
                        <Route path="/login/" component={Login} />
                        <Route path="/signup/" component={Signup} />
                        <Route exact path="/" component={HomePage} />
                    </Router>
                </React.Fragment>
            )
        }

        return (
            <React.Fragment>
            </React.Fragment>
        );
    }
}

export default Tasks;
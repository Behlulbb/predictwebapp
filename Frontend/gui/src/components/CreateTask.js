import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Form, Select, Input, Button, message, Icon } from 'antd';
import axios from 'axios';
const { Option } = Select;

class CreateTask extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tasks: props.data,
            name: '',
            datafile: '',
            prediction: '',
        }
    }

    handleChangeName = (event) => {
        this.setState({
            name: event.target.value,
        });
    }
    handleChangePredict = (event) => {
        this.setState({
            prediction: event,
        });
    }
    handleChangeFile = (event) => {
        this.setState({
            datafile: event,
        });
    }
    fetchdata() {
        const data = {
            name: this.state.name,
            datafile: this.state.datafile,
            prediction: this.state.prediction
        }
        axios({
            method: 'post',
            url: 'http://127.0.0.1:8000/api/',
            data: data,
            config: {
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                }
            }
        })
            .then((response) => {
                //handle success
                console.log(response);
                message.success('Task Created');
            })
            .catch((response) => {
                //handle error
                console.log(response);
                message.error('Task unable to create');
            });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        console.log("name:", this.state.name)
        console.log("datafile:", this.state.datafile)
        console.log("predict:", this.state.prediction)
        this.fetchdata();
    }
    onRender = () => {
        this.props.listGetAll();
    }


    render() {
        return (
            <div>
                <div className="taskname">
                    Create Tasks
                </div>
                <div className="dashboard">
                    <div className="resultheader">
                        <div className="resultheaderIcon" onClick={() => this.onRender()}>
                            <Link to={'/'}>
                                <Icon type="arrow-left" /> Back Home
                            </Link>
                        </div>
                        <Form onSubmit={this.handleSubmit}>
                            <label>
                                <div className="formName">
                                    <Input
                                        placeholder="Name"
                                        type="text"
                                        value={this.state.name} onChange={this.handleChangeName} />
                                </div>
                            </label>
                            <label>
                                <div className="form">
                                    <Select
                                        placeholder="Select data file"
                                        onChange={this.handleChangeFile}>
                                        <Option value="pizza-sales-data">pizza-sales-data.xlsx</Option>
                                    </Select>
                                </div>
                            </label>
                            <label>
                                <div className="form">
                                    <Select
                                        placeholder="Select product to predict"
                                        onChange={this.handleChangePredict}>
                                        <Option value="Neapolitan">Neapolitan pizza</Option>
                                        <Option value="Chicago">Chicago pizza</Option>
                                        <Option value="Sicilian">Sicilian pizza</Option>
                                        <Option value="Tomato Pie">Tomato Pie pizza</Option>
                                    </Select>
                                </div>
                            </label>
                            <div className="form">
                                <Button type="primary" htmlType="submit" className="form" >
                                    Submit
                            </Button>
                            </div>
                        </Form>
                    </div>
                </div>
            </div>
        );
    }
}

export default CreateTask;
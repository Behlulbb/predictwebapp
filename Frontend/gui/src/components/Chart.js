import React, { Component } from 'react';
import predictData from '../data/predictData.json';
import { LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, Legend } from 'recharts';

class Chart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: props.data,
            neopolitan: predictData.slice(0, 12),
            chicago: predictData.slice(12, 24),
            sicilian: predictData.slice(24, 36),
            tomato: predictData.slice(36, 48),
        }
    }

    render() {
        if (this.state.data.prediction === "Sicilian") {
            return (
                <div>
                    <LineChart width={1300} height={425} data={this.state.sicilian}
                        margin={{ top: 50, right: 30, left: 20, bottom: 5 }}>
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="names" />
                        <YAxis />
                        <Tooltip />
                        <Legend />
                        <Line type="monotone" dataKey="predict" stroke="#8884d8" />
                        <Line type="monotone" dataKey="orj" stroke="#82ca9d" />
                    </LineChart>
                </div>
            )
        }
        if (this.state.data.prediction === "Neapolitan") {
            return (
                <div>
                    <LineChart width={1300} height={425} data={this.state.neopolitan}
                        margin={{ top: 50, right: 30, left: 20, bottom: 5 }}>
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="names" />
                        <YAxis />
                        <Tooltip />
                        <Legend />
                        <Line type="monotone" dataKey="predict" stroke="#8884d8" />
                        <Line type="monotone" dataKey="orj" stroke="#82ca9d" />
                    </LineChart>
                </div>
            )
        }
        if (this.state.data.prediction === "Chicago") {
            return (
                <div>
                    <LineChart width={1300} height={425} data={this.state.chicago}
                        margin={{ top: 50, right: 30, left: 20, bottom: 5 }}>
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="names" />
                        <YAxis />
                        <Tooltip />
                        <Legend />
                        <Line type="monotone" dataKey="predict" stroke="#8884d8" />
                        <Line type="monotone" dataKey="orj" stroke="#82ca9d" />
                    </LineChart>
                </div>
            )
        }
        if (this.state.data.prediction === "Tomato Pie") {
            return (
                <div>
                    <LineChart width={1300} height={425} data={this.state.tomato}
                        margin={{ top: 50, right: 30, left: 20, bottom: 5 }}>
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="names" />
                        <YAxis />
                        <Tooltip />
                        <Legend />
                        <Line type="monotone" dataKey="predict" stroke="#8884d8" />
                        <Line type="monotone" dataKey="orj" stroke="#82ca9d" />
                    </LineChart>
                </div>
            )
        }
        return (

            <div>
            </div>
        );
    }
}

export default Chart;

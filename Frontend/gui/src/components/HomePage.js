import React, { Component } from 'react';
import { Icon } from 'antd';
import Login from './Login';
import Signup from './Signup';

class HomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    signupPage = () => {
        return <Signup />
    }

    render() {
        return (
            <div >
                <div className="header">
                    <div className="home">
                        <Icon type="smile" theme="twoTone" />
                    </div>
                    <div className="home">
                        Welcome to Predict Web Application
                        </div>
                </div>
                <div className="taskname">
                    Login
                </div>
                <div className="loginDash">
                    {/* <Link to={'/login/'}>Login</Link> */}
                    <Login />
                </div>
            </div>
        );
    }
}

export default HomePage;

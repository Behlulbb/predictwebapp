import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Icon } from 'antd';
import { connect } from 'react-redux';
import * as actions from '../store/actions/auth';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }


    render() {
        return (
            <div className="header">
                <div className="links">
                    <Link to={'/create'}>
                        <Icon type="plus-circle" style={{ fontSize: '16px' }} theme="twoTone" /> Create Tasks
                    </Link>
                </div>
                <div className="account" >
                    <Link to={'/'} onClick={this.props.logout}>Logout</Link>
                </div>
                <div className="account">
                    <Icon type="user" style={{ fontSize: '24px' }} />
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        username: state.userName,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        logout: () => dispatch(actions.logout()),
        onAuth: (username, password) => dispatch(actions.authLogin(username, password))
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));
import React, { Component } from 'react';
import { Icon } from 'antd';
import axios from 'axios';
import Chart from './Chart';
import { Link } from 'react-router-dom';

class Result extends Component {
    constructor(props) {
        super(props);
        this.state = {
            result: [],
        }
    }
    componentDidMount() {
        this.ResultlGet();
    }
    ResultlGet = () => {
        axios
            .get(`http://127.0.0.1:8000/api/${this.props.match.params.id}`)
            .then((response) => {
                this.setState({
                    result: response.data,
                });
            });
    }

    render() {
        const { result } = this.state;
        const Chartprops = () => <Chart data={result} />
        return (
            <div>
                <div className="taskname">
                    Result
                </div>
                <div className="dashboard">
                    <div className="resultheader">
                        <div className="resultheaderIcon">
                            <Link to={'/'}>
                                <Icon type="arrow-left" /> Back Home
                            </Link>
                        </div>
                        <div className="resultheaderIcon">
                            <span>{result.name}: {result.prediction} pizza</span>
                        </div>
                    </div>
                    <Chartprops />
                </div>
            </div>
        );
    }
}

export default Result;
